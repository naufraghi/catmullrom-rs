# Catmull-Rom Spline

This module contains a Rust implementation of the Catmull-Rom spline with a Python wrapper.

## Usage

1. Install *Rust* version 1.13+
2. Install `rust_ext` *Python* extension
3. Run the demo

### Rust

```
curl https://sh.rustup.rs -sSf | sh
```

### Python deps
```
mkvirtualenv -p $(which python3)
python -m pip install git+https://github.com/novocaine/rust-python-ext.git
python -m pip install pyqt5
python -m pip install -U .
python draw.py
```

A PyQt window should pop up with a spline on it, not editable yet.