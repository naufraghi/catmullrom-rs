from catmullrom import spline as spline_rs, P2D

class P:
    def __init__(self, x, y):
        self._p = P2D(x, y)
    @property
    def x(self):
        return self._p.x()
    @property
    def y(self):
        return self._p.y()
    def __repr__(self):
        return repr(self._p)

def spline(points, eps=0.5, closed=False):
    r, line = spline_rs([p._p for p in points], eps, closed)
    return r, [P(p.x(), p.y()) for p in line]