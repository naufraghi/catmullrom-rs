#!/bin/env python3

from PyQt5.QtCore import QSize, QPointF
from PyQt5.QtGui import QPainter, QPolygonF
from PyQt5.QtWidgets import QWidget, QApplication

from pycatmullrom import P, spline


class Frame(QWidget):
    def __init__(self, parent=None):
        points = [P(0, 0), P(0, 0),
                  P(100, 200),
                  P(200, 200),
                  P(300, 400),
                  P(400, 400),
                  P(500, 600), P(500, 600)]
        super().__init__(parent)
        self.setState(points)

    def setState(self, points):
        self.points = QPolygonF([QPointF(p.x, p.y) for p in points])
        r, line = spline(points, 0.5, False)
        print("line", len(line))
        print("r", len(r), min(r), max(r))
        self.poly = QPolygonF([QPointF(p.x, p.y) for p in line])
        self.update()

    def sizeHint(self):
        return QSize(600, 600)

    def minimumSizeHint(self):
        return QSize(100, 100)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.drawPolyline(self.poly)
        pen = painter.pen()
        pen.setWidth(5)
        painter.setPen(pen)
        painter.drawPoints(self.points)


if __name__ == "__main__":
    import sys
    app = QApplication([])
    f = Frame()
    f.show()
    sys.exit(app.exec_())
