// A library to compute Catmull-Rom interpolating splines
// Reference: https://github.com/empet/Geometric-Modeling/blob/master/Catmull-Rom-splines.ipynb
use std::ops::{Mul, Add, Sub};

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Point2D {
    x: f32,
    y: f32,
}

impl Point2D {
    fn new(x: f32, y: f32) -> Self {
        Point2D { x: x, y: y }
    }
    fn norm2(self) -> f32 {
        self.x * self.x + self.y * self.y
    }
    fn norm(self) -> f32 {
        self.norm2().sqrt()
    }
    fn dist(self, other: Point2D) -> f32 {
        (self - other).norm()
    }
}

impl Mul<f32> for Point2D {
    type Output = Point2D;
    fn mul(self, other: f32) -> Point2D {
        Point2D {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl Add for Point2D {
    type Output = Point2D;
    fn add(self, other: Point2D) -> Point2D {
        Point2D {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point2D {
    type Output = Point2D;
    fn sub(self, other: Point2D) -> Point2D {
        Point2D {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

struct CubicPoly {
    c0: f32,
    c1: f32,
    c2: f32,
    c3: f32,
}

impl CubicPoly {
    // https://stackoverflow.com/a/23980479/2929337
    fn new(x0: f32, x1: f32, t0: f32, t1: f32) -> Self {
        let c0 = x0;
        let c1 = t0;
        let c2 = -3. * x0 + 3. * x1 - 2. * t0 - t1;
        let c3 = 2. * x0 - 2. * x1 + t0 + t1;
        CubicPoly {
            c0: c0,
            c1: c1,
            c2: c2,
            c3: c3,
        }
    }
    fn eval(&self, t: f32) -> f32 {
        let t2 = t * t;
        let t3 = t2 * t;
        self.c0 + self.c1 * t + self.c2 * t2 + self.c3 * t3
    }
}

pub struct CatmullRom2DSingle {
    cubic_poly_x: CubicPoly,
    cubic_poly_y: CubicPoly,
}

impl CatmullRom2DSingle {
    // compute coefficients for a nonuniform Catmull-Rom spline
    fn cubic_poly(x0: f32, x1: f32, x2: f32, x3: f32, dt0: f32, dt1: f32, dt2: f32) -> CubicPoly {
        // compute tangents when parameterized in [t1,t2]
        let t1 = (x1 - x0) / dt0 - (x2 - x0) / (dt0 + dt1) + (x2 - x1) / dt1;
        let t2 = (x2 - x1) / dt1 - (x3 - x1) / (dt1 + dt2) + (x3 - x2) / dt2;

        // rescale tangents for parametrization in [0,1]
        CubicPoly::new(x1, x2, t1 * dt1, t2 * dt1)
    }
    fn eval(&self, t: f32) -> Point2D {
        Point2D::new(self.cubic_poly_x.eval(t), self.cubic_poly_y.eval(t))
    }
    fn new(p0: Point2D, p1: Point2D, p2: Point2D, p3: Point2D, alpha: f32) -> CatmullRom2DSingle {
        let mut dt0 = (p0 - p1).norm().powf(alpha);
        let mut dt1 = (p1 - p2).norm().powf(alpha);
        let mut dt2 = (p2 - p3).norm().powf(alpha);

        // safety check for repeated Point2Ds
        if dt1 < 1e-4 {
            dt1 = 1.0;
        }
        if dt0 < 1e-4 {
            dt0 = dt1;
        }
        if dt2 < 1e-4 {
            dt2 = dt1;
        }

        CatmullRom2DSingle {
            cubic_poly_x: CatmullRom2DSingle::cubic_poly(p0.x, p1.x, p2.x, p3.x, dt0, dt1, dt2),
            cubic_poly_y: CatmullRom2DSingle::cubic_poly(p0.y, p1.y, p2.y, p3.y, dt0, dt1, dt2),
        }
    }
}

macro_rules! assert_le {
    ($left:expr , $right:expr) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(*left_val <= *right_val) {
                    panic!("assertion failed: `(left <= right)` \
                           (left: `{:?}`, right: `{:?}`)", left_val, right_val)
                }
            }
        }
    });
    ($left:expr , $right:expr, $($arg:tt)*) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val <= *right_val) {
                    panic!("assertion failed: `(left <= right)` \
                           (left: `{:?}`, right: `{:?}`): {}", left_val, right_val,
                           format_args!($($arg)*))
                }
            }
        }
    });
}


#[test]
fn test_catmullrom2d_single() {
    use std::f32;
    //  ^
    //  |        /
    //  |       /
    //  |  .-._.
    //  | /
    //  |/
    // --------------->
    //  |
    let p0 = Point2D::new(0., 0.);
    let p1 = Point2D::new(1., 2.);
    let p2 = Point2D::new(2., 2.);
    let p3 = Point2D::new(3., 4.);
    let cr = CatmullRom2DSingle::new(p0, p1, p2, p3, 0.5);
    // central Point2D is in the middle, ad on the p1-p2 line
    assert_le!((cr.eval(0.5).x - (1. + 2.) / 2.).abs(), f32::EPSILON);
    assert_le!((cr.eval(0.5).y - 2.).abs(), f32::EPSILON);
    // the left side is upper than the right side
    assert_le!(cr.eval(0.75).y, cr.eval(0.25).y);
}

pub struct CatmullRom2D {
    pts: Vec<Point2D>,
    poly: Vec<CatmullRom2DSingle>,
}

impl CatmullRom2D {
    fn new(pts: Vec<Point2D>, alpha: f32, closed: bool) -> Option<CatmullRom2D> {
        if pts.len() < 4 {
            None
        } else {
            let mut pts = pts;
            if closed {
                let head = &pts.clone()[..3];
                pts.extend_from_slice(head);
            }
            let poly: Vec<CatmullRom2DSingle> = pts[..]
                .windows(4)
                .map(|pts| CatmullRom2DSingle::new(pts[0], pts[1], pts[2], pts[3], alpha))
                .collect();
            Some(CatmullRom2D {
                pts: pts,
                poly: poly,
            })
        }
    }
    fn eval(&self, t: f32) -> Point2D {
        let cap = |i| f32::min(f32::max(0., i), (self.poly.len() - 1) as f32);
        let i = cap(t.floor());
        self.poly[i as usize].eval(t - i)
    }
    fn geom(&self, n: usize) -> (Vec<f32>, Vec<Point2D>) {
        let r = self.poly.len() as f32;
        let steps: Vec<f32> = (0..n + 1)
            .map(|i| r * (i as f32) / (n as f32))
            .collect();
        let line: Vec<Point2D> = steps.iter()
            .map(|t| self.eval(*t))
            .collect();
        // let mut range: Vec<f32> = line[..]
        // .windows(2)
        // .scan(0., |state, d| {
        // state = *state + d[0].dist(d[1]);
        // Some(*state)
        // })
        // .collect();
        // range.insert(0, 0_f32);
        // assert!(range.len() == line.len());
        (steps, line)
    }
    fn steps(&self, eps: f32) -> usize {
        (len(&self.pts[..]) / eps) as usize
    }
    fn interpolate(&self, eps: f32) -> (Vec<f32>, Vec<Point2D>) {
        self.geom(self.steps(eps))
    }
}

fn len(pts: &[Point2D]) -> f32 {
    pts.windows(2).fold(0., |sum, d| (d[0]).dist(d[1]) + sum)
}

#[test]
fn test_catmullrom2d() {
    use std::f32;
    //  ^              .
    //  |             /'
    //  |        ..__/ '
    //  |       /'  '  '
    //  |  ..__/ '  '  '
    //  | /      '  '  '
    //  |/       '  '  '
    // -+--------------------->
    // [0  1  2  3] '  '        t [0,1]
    //    [1  2  3  4] '        t [1,2]
    //       [2  3  4  5]       t [2,3]
    let pts = vec![Point2D::new(0., 0.),
                   Point2D::new(1., 2.),
                   Point2D::new(2., 2.),
                   Point2D::new(3., 4.),
                   Point2D::new(4., 4.),
                   Point2D::new(5., 6.)];
    if let Some(cr) = CatmullRom2D::new(pts, 0.5, true) {
        // first central Point2D is in the middle, ad on the p1-p2 line
        assert_le!((cr.eval(0.5).x - (1. + 2.) / 2.).abs(),
                   f32::EPSILON,
                   "first step");
        assert_le!((cr.eval(0.5).y - 2.).abs(), f32::EPSILON, "first step");
        // the left side is upper than the right side
        assert_le!(cr.eval(0.75).y, cr.eval(0.25).y);
        // the same for the second step
        assert_le!((cr.eval(2. + 0.5).x - (3. + 4.) / 2.).abs(),
                   f32::EPSILON,
                   "second step");
        assert_le!((cr.eval(2. + 0.5).y - 4.).abs(),
                   f32::EPSILON,
                   "second step");
        // the left side is upper than the right side
        assert_le!(cr.eval(2. + 0.75).y, cr.eval(2. + 0.25).y);
    } else {
        unreachable!("Unable to create a new CatmullRom2D");
    }
}

#[test]
fn test_catmullrom2d_geom() {
    //  ^              .
    //  |             /'
    //  |        ..__/ '
    //  |       /'  '  '
    //  |  ..__/ '  '  '
    //  | /      '  '  '
    //  |/       '  '  '
    // -+--------------------->
    // [0  1  2  3] '  '        t [0,1]
    //    [1  2  3  4] '        t [1,2]
    //       [2  3  4  5]       t [2,3]
    let pts = vec![Point2D::new(0., 0.),
                   Point2D::new(1., 2.),
                   Point2D::new(2., 2.),
                   Point2D::new(3., 4.),
                   Point2D::new(4., 4.),
                   Point2D::new(5., 6.)];
    if let Some(cr) = CatmullRom2D::new(pts, 0.5, false) {
        let (_, spline) = cr.interpolate(0.4);
        for p in spline {
            println!("{}, {}", p.x, p.y);
        }
    } else {
        unreachable!("Unable to create a new CatmullRom2D");
    }
}

/// /////////////////////////////////////////////////////////////////////////////
/// Python module
/// /////////////////////////////////////////////////////////////////////////////
#[macro_use]
extern crate cpython;

use cpython::{PyResult, Python, PyErr, exc};


py_module_initializer!(catmullrom, initcatmullrom, PyInit_catmullrom, |py, m| {
    try!(m.add(py, "__doc__", "Rust Catmull-Rom spline."));
    try!(m.add_class::<P2D>(py));
    try!(m.add(py,
               "spline",
               py_fn!(py, py_spline(points: Vec<P2D>, eps: f32, closed: bool))));
    Ok(())
});

fn py_spline(py: Python,
             points: Vec<P2D>,
             eps: f32,
             closed: bool)
             -> PyResult<(Vec<f32>, Vec<P2D>)> {
    let pts = points.iter().map(|p| *p.point(py)).collect();
    if let Some(cr) = CatmullRom2D::new(pts, eps, closed) {
        let (range, line) = cr.interpolate(eps);
        let spline = line.iter().map(|p| P2D::create_instance(py, *p).unwrap()).collect();
        Ok((range, spline))
    } else {
        Err(PyErr::new_lazy_init(py.get_type::<exc::ValueError>(), None))
    }
}

py_class!(class P2D |py| {
    data point: Point2D;
    def __new__(_cls, x: f32, y: f32) -> PyResult<P2D> {
        P2D::create_instance(py, Point2D::new(x, y))
    }
    def __repr__(&self) -> PyResult<String> {
        let p = self.point(py);
        Ok(format!("P({}, {})", p.x, p.y))
    }
    def x(&self) -> PyResult<f32> {
        Ok(self.point(py).x)
    }
    def y(&self) -> PyResult<f32> {
        Ok(self.point(py).y)
    }
});